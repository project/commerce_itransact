<?php

namespace Drupal\commerce_itransact\Plugin\Commerce\PaymentGateway;

use Drupal\Component\Datetime\TimeInterface;
use Drupal\Core\Database\Connection;
use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\TempStore\PrivateTempStoreFactory;
use Drupal\Core\Session\SessionManagerInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\commerce_payment\CreditCard;
use Drupal\commerce_payment\Entity\PaymentInterface;
use Drupal\commerce_payment\Entity\PaymentMethodInterface;
use Drupal\commerce_payment\Exception\HardDeclineException;
use Drupal\commerce_payment\Exception\PaymentGatewayException;
use Drupal\commerce_payment\PaymentMethodTypeManager;
use Drupal\commerce_payment\PaymentTypeManager;
use Drupal\commerce_payment\Plugin\Commerce\PaymentGateway\OnsitePaymentGatewayBase;
use Drupal\commerce_price\Price;
use Drupal\user\UserInterface;
use GuzzleHttp\ClientInterface;
use GuzzleHttp\Exception\GuzzleException;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\Session\SessionInterface;

/**
 * Provides the iTransact payment gateway.
 *
 * @CommercePaymentGateway(
 *   id = "itransact",
 *   label = @Translation("iTransact"),
 *   display_label = @Translation("iTransact"),
 *   payment_method_types = {"credit_card"},
 *   requires_billing_information = TRUE,
 *   forms = {
 *     "add-payment-method" = "Drupal\commerce_itransact\PluginForm\Onsite\PaymentMethodAddForm",
 *     "edit-payment-method" = "Drupal\commerce_payment\PluginForm\PaymentMethodEditForm",
 *   },
 *   credit_card_types = {
 *     "amex", "dinersclub", "discover", "mastercard", "visa",
 *   },
 * )
 */
class Onsite extends OnsitePaymentGatewayBase implements OnsiteInterface {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The ID of the parent config entity.
   *
   * Not available while the plugin is being configured.
   *
   * @var string
   */
  protected $entityId;

  /**
   * The payment type used by the gateway.
   *
   * @var \Drupal\commerce_payment\Plugin\Commerce\PaymentType\PaymentTypeInterface
   */
  protected $paymentType;

  /**
   * The payment method types handled by the gateway.
   *
   * @var \Drupal\commerce_payment\Plugin\Commerce\PaymentMethodType\PaymentMethodTypeInterface[]
   */
  protected $paymentMethodTypes;

  /**
   * The time.
   *
   * @var \Drupal\Component\Datetime\TimeInterface
   */
  protected $time;

  /**
   * The entity field manager.
   *
   * @var \Drupal\Core\Entity\EntityFieldManagerInterface
   */
  protected $entityFieldManager;

  /**
   * The Guzzle HTTP client.
   *
   * @var \GuzzleHttp\ClientInterface
   */
  protected $httpClient;

  /**
   * The logger instance.
   *
   * @var \Psr\Log\LoggerInterface
   */
  protected $logger;

  /**
   * @var \Drupal\Core\Session\SessionManagerInterface
   */
  private $sessionManager;

  /**
   * @var \Drupal\Core\TempStore\PrivateTempStoreFactory
   */
  protected $tempStore;

  /**
   * @var \Symfony\Component\HttpFoundation\Session\SessionInterface
   */
  protected $session;

  /**
   * The API base URL.
   */
  const API_BASE_URL = 'https://api.itransact.com';

  /**
   * The database connection.
   *
   * @var Drupal\Core\Database\Connection
   */
  protected $connection;

  /**
   * The request stack.
   *
   * @var \Symfony\Component\HttpFoundation\RequestStack
   */
  protected $requestStack;
 
  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, EntityTypeManagerInterface $entity_type_manager, PaymentTypeManager $payment_type_manager, PaymentMethodTypeManager $payment_method_type_manager, TimeInterface $time, EntityFieldManagerInterface $entity_field_manager, ClientInterface $http_client, LoggerChannelFactoryInterface $logger_factory, PrivateTempStoreFactory $temp_store_factory, SessionManagerInterface $session_manager, SessionInterface $session, Connection $connection, RequestStack $request_stack) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $entity_type_manager, $payment_type_manager, $payment_method_type_manager, $time);

    $this->entityFieldManager = $entity_field_manager;
    $this->httpClient = $http_client;
    $this->logger = $logger_factory->get('commerce_itransact');
    $this->tempStore = $temp_store_factory->get('commerce_itransact');
    $this->sessionManager = $session_manager;
    $this->session = $session;
    $this->connection = $connection;
    $this->requestStack = $request_stack;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager'),
      $container->get('plugin.manager.commerce_payment_type'),
      $container->get('plugin.manager.commerce_payment_method_type'),
      $container->get('datetime.time'),
      $container->get('entity_field.manager'),
      $container->get('http_client'),
      $container->get('logger.factory'),
      $container->get('tempstore.private'),
      $container->get('session_manager'),
      $container->get('session'),
      $container->get('database'),
      $container->get('request_stack')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'api_username' => '',
      'api_key' => '',
      'options' => [
        'log' => [],
      ],
      'flood_control' => [
        'maximum' => 5,
        'seconds' => 300,
      ],
    ] + parent::defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);

    $form['api_username'] = [
      '#type' => 'textfield',
      '#title' => $this->t('API username'),
      '#default_value' => $this->configuration['api_username'],
      '#description' => $this->t('API username obtained from: @url', ['@url' => 'https://gateway.itransact.com/h/cp/merchant_settings/edit_integration']),
      '#required' => TRUE,
    ];

    $form['api_key'] = [
      '#type' => 'textfield',
      '#title' => $this->t('API key'),
      '#default_value' => $this->configuration['api_key'],
      '#description' => $this->t('API key obtained from: @url', ['@url' => 'https://gateway.itransact.com/h/cp/merchant_settings/edit_integration']),
      '#required' => TRUE,
    ];

    $form['flood_control'] = [
      '#type' => 'details',
      '#title' => $this->t('Flood control'),
      '#description' => $this->t('Limits orders to a maximum number within a time period.'),
    ];

    $form['flood_control']['maximum'] = [
      '#type' => 'textfield',
      '#attributes' => [
        'type' => 'number',
      ],
      '#title' => $this->t('Maximum allowed orders'),
      '#required' => TRUE,
      '#maxlength' => 3,
      '#default_value' => $this->configuration['flood_control']['maximum'],
    ];

    $form['flood_control']['seconds'] = [
      '#type' => 'textfield',
      '#attributes' => [
        'type' => 'number',
      ],
      '#title' => $this->t('Time period (in seconds)'),
      '#required' => TRUE,
      '#maxlength' => 4,
      '#default_value' => $this->configuration['flood_control']['seconds'],
    ];

    $form['options'] = [
      '#type' => 'details',
      '#title' => $this->t('API options'),
      '#description' => $this->t('Additional options for API communication'),
    ];

    $form['options']['log'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('Log the following messages for debugging'),
      '#options' => [
        'request' => $this->t('API request messages'),
        'response' => $this->t('API response messages'),
      ],
      '#default_value' => $this->configuration['options']['log'],
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateConfigurationForm(array &$form, FormStateInterface $form_state) {
    parent::validateConfigurationForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    parent::submitConfigurationForm($form, $form_state);

    if (!$form_state->getErrors()) {
      $values = $form_state->getValue($form['#parents']);
      $this->configuration['api_username'] = $values['api_username'];
      $this->configuration['api_key'] = $values['api_key'];
      $this->configuration['options']['log'] = $values['options']['log'];
      $this->configuration['flood_control']['maximum'] = $values['flood_control']['maximum'];
      $this->configuration['flood_control']['seconds'] = $values['flood_control']['seconds'];
    }
  }

  /**
   * {@inheritdoc}
   */
  public function createPayment(PaymentInterface $payment, $capture = TRUE) {
    $this->assertPaymentState($payment, ['new']);
    $payment_method = $payment->getPaymentMethod();
    $this->assertPaymentMethod($payment_method);

    // Get the number of orders from this IP address in the past X seconds.
    $time = $this->time->getRequestTime();
    $ip_address = $this->requestStack->getCurrentRequest()->getClientIp();
    $count = $this->connection->select('commerce_order', 'co')
      ->condition('co.ip_address', $ip_address)
      ->condition('co.created', [($time - $this->configuration['flood_control']['seconds']), $time], 'BETWEEN')
      ->condition('co.payment_gateway', $payment->getPaymentGatewayId())
      ->countQuery()
      ->execute()
      ->fetchField();

    if ($count > $this->configuration['flood_control']['maximum']) {
      $error = $this->t('iTransact error: Greater than @attempts order attempts within @seconds seconds.', [
        '@attempts' => $this->configuration['flood_control']['maximum'],
        '@seconds' => $this->configuration['flood_control']['seconds'],
      ]);
      throw new PaymentGatewayException($error);
    }

    // Prepare all the required values for the iTransact "/transactions" POST.
    $amount = $payment->getAmount();
    $order = $payment->getOrder();
    $email = $order->getEmail();
    $owner = $payment_method->getOwner();

    // Handle the owner.
    $customer_id = NULL;
    if ($owner && $owner->isAuthenticated()) {
      $customer_id = $this->getRemoteCustomerId($owner);
      if (!$customer_id) {
        $this->setRemoteCustomerId($owner, $this->tempStore->get('customer_id'));
        $owner->save();
      }
    }

    if (!$customer_id) {
      $customer_id = $this->tempStore->get('customer_id');
    }

    $payload = [
      'customer_id' => $customer_id,
      'payment_source_id' => $payment_method->getRemoteId(),
      'amount' => $this->toMinorUnits($amount),
      'capture' => $capture,
      // The iTransact UI displays this as "User ID" even though
      // the variable name is order_number.
      'order_number' => $owner->id(),
      'metadata' => [
        'email' => $email,
        'order_number' => $payment->getOrderId(),
      ],
    ];

    $request = [
      'json' => $payload,
      'headers' => $this->generateHeaders($payload),
    ];
    $this->logRequest($request, '/transactions');

    // Make/catch the request.
    try {
      $response = $this->httpClient->post(static::API_BASE_URL . '/transactions', $request);
    }
    catch (GuzzleException $e) {

      // @TODO: At this point, is the payment method still valid? Should we
      // remove it so the customer is required to supply new card info?

      $response = json_decode($e->getResponse()->getBody());
      $error = $this->t('iTransact error: @code - @error', [
        '@code' => $e->getResponse()->getStatusCode(),
        '@error' => $response->error->message,
      ]);

      switch ($e->getResponse()->getStatusCode()) {

        case 402:
          throw new HardDeclineException($error, $e->getResponse()->getStatusCode());
          break;
          
        default:
          throw new PaymentGatewayException($error, $e->getResponse()->getStatusCode());
          break;

      }
    }

    // Retrieve the values needed from the response.
    $contents = json_decode($response->getBody()->getContents());
    $this->logResponse($contents, '/transactions');

    $payment->setRemoteId($contents->id);

    $next_state = $capture ? 'completed' : 'authorization';

    $payment->setState($next_state);
    $payment->save();
  }

  /**
   * {@inheritdoc}
   */
  public function capturePayment(PaymentInterface $payment, Price $amount = NULL) {
    $this->assertPaymentState($payment, ['authorization']);
    // If not specified, capture the entire amount.
    $amount = $amount ?: $payment->getAmount();

    // Perform the capture request here, throw an exception if it fails.
    // See \Drupal\commerce_payment\Exception for the available exceptions.
    $remote_id = $payment->getRemoteId();
    $number = $amount->getNumber();

    $payment->setState('completed');
    $payment->setAmount($amount);
    $payment->save();
  }

  /**
   * {@inheritdoc}
   */
  public function voidPayment(PaymentInterface $payment) {
    $this->assertPaymentState($payment, ['authorization']);
    // Perform the void request here, throw an exception if it fails.
    // See \Drupal\commerce_payment\Exception for the available exceptions.
    $remote_id = $payment->getRemoteId();

    $payment->setState('authorization_voided');
    $payment->save();
  }

  /**
   * {@inheritdoc}
   */
  public function refundPayment(PaymentInterface $payment, Price $amount = NULL) {
    $this->assertPaymentState($payment, ['completed', 'partially_refunded']);
    // If not specified, refund the entire amount.
    $amount = $amount ?: $payment->getAmount();
    $this->assertRefundAmount($payment, $amount);

    // Perform the refund request here, throw an exception if it fails.
    // See \Drupal\commerce_payment\Exception for the available exceptions.
    $remote_id = $payment->getRemoteId();
    $number = $amount->getNumber();

    $old_refunded_amount = $payment->getRefundedAmount();
    $new_refunded_amount = $old_refunded_amount->add($amount);
    if ($new_refunded_amount->lessThan($payment->getAmount())) {
      $payment->setState('partially_refunded');
    }
    else {
      $payment->setState('refunded');
    }

    $payment->setRefundedAmount($new_refunded_amount);
    $payment->save();
  }

  /**
   * {@inheritdoc}
   */
  public function createPaymentMethod(PaymentMethodInterface $payment_method, array $payment_details) {
    // Required values from the payment form.
    $required_keys = [
      'type', 'number', 'expiration', 'security_code',
    ];
    foreach ($required_keys as $required_key) {
      if (empty($payment_details[$required_key])) {
        throw new \InvalidArgumentException(sprintf('$payment_details must contain the %s key.', $required_key));
      }
    }

    // Make sure we have the billing address needed to create the token.
    /** @var \Drupal\address\Plugin\Field\FieldType\AddressItem $billing_address */
    if ($billing_address = $payment_method->getBillingProfile()) {
      $billing_address = $payment_method->getBillingProfile()->get('address')->first();
    }
    else {
      throw new HardDeclineException('No billing address defined for the payment');
    }

    // Prepare all the required values for the iTransact "/tokens" POST.
    $payload = [
      'card' => [
        'name' => $billing_address->getGivenName() . ' ' . $billing_address->getFamilyName(),
        'number' => (int) $payment_details['number'],
        // Don't use an (int) for cvv as leading zeros are valid.
        'cvv' => $payment_details['security_code'],
        'exp_month' => (int) $payment_details['expiration']['month'],
        'exp_year' => (int) $payment_details['expiration']['year'],
      ],
      'address' => [
        'line1' => $billing_address->getAddressLine1(),
        'line2' => $billing_address->getAddressLine2(),
        'city' => $billing_address->getLocality(),
        'state' => $billing_address->getAdministrativeArea(),
        'postal_code' => $billing_address->getPostalCode(),
        'country' => $billing_address->getCountryCode(),
      ],
    ];

    $request = [
      'json' => $payload,
      'headers' => $this->generateHeaders($payload),
    ];
    $this->logRequest($request, '/tokens');

    // Make/catch the request.
    try {
      $response = $this->httpClient->post(static::API_BASE_URL . '/tokens', $request);
    }
    catch (GuzzleException $e) {
      $response = json_decode($e->getResponse()->getBody());
      $error = $this->t('iTransact error: @code - @error', [
        '@code' => $e->getResponse()->getStatusCode(),
        '@error' => $response->error->message,
      ]);
      throw new PaymentGatewayException($error, $e->getResponse()->getStatusCode());
    }

    // We have a token, now create the customer.
    // Add the token to the payload to attach the payment to the new customer.
    $contents = json_decode($response->getBody()->getContents());
    $this->logResponse($contents, '/tokens');

    $payload['token'] = $contents->token;
    unset($payload['card']);

    $request = [
      'json' => $payload,
      'headers' => $this->generateHeaders($payload),
    ];
    $this->logRequest($request, '/customers');

    // Make/catch the request.
    try {
      $response = $this->httpClient->post(static::API_BASE_URL . '/customers', $request);
    }
    catch (GuzzleException $e) {
      $response = json_decode($e->getResponse()->getBody());
      $error = $this->t('iTransact error: @code - @error', [
        '@code' => $e->getResponse()->getStatusCode(),
        '@error' => $response->error->message,
      ]);
      throw new PaymentGatewayException($error, $e->getResponse()->getStatusCode());
    }

    $contents = json_decode($response->getBody()->getContents());
    $this->logResponse($contents, '/customers');

    // Standard stuff here.
    $payment_method->card_type = $payment_details['type'];
    $payment_method->card_number = substr($payment_details['number'], -4);
    $payment_method->card_exp_month = $payment_details['expiration']['month'];
    $payment_method->card_exp_year = $payment_details['expiration']['year'];
    $expires = CreditCard::calculateExpirationTimestamp($payment_details['expiration']['month'], $payment_details['expiration']['year']);
    $payment_method->setExpiresTime($expires);

    // Now we need to store the customer_id so that it can be used in createPayment()
    // and, in the case of annonymous customers, again in the membership checkout pane.
    $owner = $payment_method->getOwner();
    // if ($owner->isAnonymous() && !$this->sessionManager->isStarted()) {
    //   $this->session->migrate();
    // }
    $this->tempStore->set('customer_id', $contents->id);

    // Handle the owner.
    if ($owner && $owner->isAuthenticated()) {
      // Logged in customers.
      $customer_id = $this->getRemoteCustomerId($owner);
      if (!$customer_id) {
        $this->setRemoteCustomerId($owner, $contents->id);
        $owner->save();
      }
    }

    $payment_method->setRemoteId($contents->payment_sources[0]->id);
    $payment_method->save();
  }

  /**
   * {@inheritdoc}
   */
  public function deletePaymentMethod(PaymentMethodInterface $payment_method) {
    // Delete the remote record here, throw an exception if it fails.
    // See \Drupal\commerce_payment\Exception for the available exceptions.
    // Delete the local entity.
    $payment_method->delete();
  }

  /**
   * {@inheritdoc}
   */
  public function updatePaymentMethod(PaymentMethodInterface $payment_method) {
    // The default payment method edit form only supports updating billing info.
    $billing_profile = $payment_method->getBillingProfile();

    // Perform the update request here, throw an exception if it fails.
    // See \Drupal\commerce_payment\Exception for the available exceptions.
  }

  /**
   * {@inheritdoc}
   */
  private function generateHeaders(array $payload) {
    $encodedUsername = base64_encode($this->configuration['api_username']);

    $digest = hash_hmac('sha256', json_encode($payload), $this->configuration['api_key'], TRUE);
    $payloadSignature = base64_encode($digest);

    return [
      'Content-Type' => 'application/json',
      'Authorization' => $encodedUsername . ':' . $payloadSignature,
    ];
  }

  /**
   * Logs the request data.
   */
  private function logRequest($request, $url) {
    if (!empty($this->configuration['options']['log']['request'])) {
      $this->logger->info('@url request: @message', ['@url' => $url, '@message' => print_r($request, TRUE)]);
    }
  }

  /**
   * Logs the response data.
   */
  private function logResponse($response, $url) {
    if (!empty($this->configuration['options']['log']['response'])) {
      $this->logger->info('@url response: @message', ['@url' => $url, '@message' => print_r($response, TRUE)]);
    }
  }

}
